import ua.com.autoview.MySearchWords;
import ua.com.autoview.ResultEvent;
import ua.com.autoview.ResultListener;

import java.net.URL;
import java.util.concurrent.CopyOnWriteArraySet;

public class Test implements ResultListener {
    private static CopyOnWriteArraySet<URL> rezArr;

    @Override
    public void resultSearch(ResultEvent event) {
        this.rezArr = event.rezArr;
    }

    public static void main(String[] args) {
        MySearchWords mySearchWords = new MySearchWords();
        String[] arrURL = {
                "http://autoview.com.ua/",
                "http://aukro.ua/",
                "http://carcamera.com.ua/"


        };

        String[] sArr = {
                "X-vision",
                "Gazer",



        };


        Test test = new Test();
        mySearchWords.addResultListener(test);
        mySearchWords.setArrUrl(arrURL);
        mySearchWords.setArrSearch(sArr);
        mySearchWords.setArrSearch("First");
        mySearchWords.setArrSearch("Second");
        mySearchWords.setArrSearch("Third");
        mySearchWords.setLimit(2);
        mySearchWords.start();


        System.out.println("Words found in:");
        for (URL q:rezArr){
            System.out.println(q);
        }
    }
}
