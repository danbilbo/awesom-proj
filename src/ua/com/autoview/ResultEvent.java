package ua.com.autoview;

import java.net.URL;
import java.util.EventObject;
import java.util.concurrent.CopyOnWriteArraySet;

public class ResultEvent extends EventObject {
    public CopyOnWriteArraySet<URL> rezArr;

    public ResultEvent(Object source, CopyOnWriteArraySet<URL> rezArr) {
        super(source);
        this.rezArr = (CopyOnWriteArraySet<URL>) source;
    }

    public ResultEvent(Object source) {
        super(source);
        this.rezArr = (CopyOnWriteArraySet<URL>) source;
    }
}
