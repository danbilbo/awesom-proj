package ua.com.autoview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Phaser;


public class ReadPageContent implements Runnable {
    private String adress = "http://autoview.com.ua/";
    private URL url;
    private String[] arrSearch;
    private boolean foundW = false;
    URLConnection urlConnection;//todo
    BufferedReader pageReader;
    Set<URL> urlSet = new HashSet<>();
    URL tempURL;
    String str = "";
    int stage;
    Phaser phaser;
    boolean flag = true;

    public ReadPageContent(URL url, int st, Phaser ph, String[] arr) {
        this.url = url;
        stage = st;
        phaser = ph;
        arrSearch = arr;
        phaser.register();
    }

    public ReadPageContent(String adress, int st, Phaser ph, String[] arr) {
        this.adress = adress;
        stage = st;
        phaser = ph;
        arrSearch = arr;
        phaser.register();
    }

    @Override
    public void run() {
        readPageContent();
    }

    private void readPageContent() {
        try {
            if (url == null) {
                url = new URL(adress);
            }
            urlConnection = url.openConnection();
            pageReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            while (str != null) {
                str = pageReader.readLine();
                if (str == null) {
                    break
                }

                if (flag) {

                    for (int i = 0; i < arrSearch.length; i++) {
                        if (str.contains(arrSearch[i])) {
//                            System.out.println("Word " + arrSearch[i] + " is founded in " + url);
                            flag = false;
                            MySearchWords.UrlWords.add(url);
//                            System.out.println(url + " added");
                        }
                    }
                }
                if (str.contains("href=\"")) {//todo " and condition
                    if (!str.contains("href=\"#")) {
                        checkAddUrl(str);
                    }
                }
            }
        } catch (MalformedURLException | NullPointerException e) {/*NAN*/}//todo logging and cascade catch
        finally {
            if (pageReader != null) {
                try {
                    pageReader.close();
                } catch (IOException e) { /*NAN*/ }
            }
        }

        for (URL s : urlSet) {
            if ((stage) < MySearchWords.getLimit()) {
                if (MySearchWords.chackedUrl.add(s)) {
                    MySearchWords.executor.execute(new ReadPageContent(s, stage + 1, phaser, arrSearch));
                }
            }
        }
        phaser.arriveAndDeregister();
    }

    private void checkAddUrl(String line) {
        int start, fin;
        String mainUrl = url.getProtocol() + "://" + url.getHost();

        start = line.indexOf("href=") + 6;
        fin = line.indexOf("\"", start);
        line = line.substring(start, fin);

        if (line.contains(".css")) return;
        if (line.contains(".jpg")) return;
        if (line.contains(".ico")) return;
        if (line.contains(".png")) return;

        if (line.startsWith("/")) {
            line = mainUrl + line;
        } else if (!line.startsWith("http")) {
            line = mainUrl + "/" + line;
        }

        try {
            if (!MySearchWords.isOutCheck()) {
                if (line.contains(url.getHost())) {
                    tempURL = new URL(line);
                }
            } else {
                tempURL = new URL(line);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        urlSet.add(tempURL);
//        System.out.println(tempURL+ " added");
    }
}
