package ua.com.autoview;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Phaser;

public class MySearchWords implements Serializable {

    private String[] arrSearch;
    private String[] arrUrl;
    private static int limit = 1;                                              //depth of searching. Defaul limit = 1
    private URL tempURL;
    private static boolean outCheck = false;                                  //for searching out of selected web-sites choose "true"
    static ExecutorService executor = Executors.newFixedThreadPool(10);
    static CopyOnWriteArraySet<URL> chackedUrl = new CopyOnWriteArraySet<>();
    static CopyOnWriteArraySet<URL> UrlWords = new CopyOnWriteArraySet<>();
    private static Phaser mainPhaser;
    private ArrayList<ResultListener> listeners = new ArrayList<>();

    public MySearchWords() {
    }


    public String[] getArrSearch() {
        return arrSearch;
    }

    public void setArrSearch(String[] arrSearch) {
        this.arrSearch = arrSearch;
        System.out.println("Array added successfully");
    }

    public void setArrSearch(String search) {
        if (arrSearch == null) {
            System.out.println("Create array for search");
            arrSearch = new String[1];
            arrSearch[0] = search;
        } else {
            String[] arrSearchTmp = (String[]) arrSearch.clone();
            arrSearch = new String[arrSearchTmp.length + 1];
            System.arraycopy(arrSearchTmp, 0, arrSearch, 0, arrSearchTmp.length);
            arrSearch[arrSearch.length - 1] = search;
            arrSearchTmp = null;
            System.out.println("Word " + search + " added to array for search successfully");
        }
    }

    public void addResultListener(ResultListener rs) {
        listeners.add(rs);
    }

    public void removeResultListener(ResultListener rs) {
        listeners.remove(rs);
    }

    public String[] getArrUrl() {
        return arrUrl;
    }

    public void setArrUrl(String[] arrUrl) {
        this.arrUrl = arrUrl;
    }


    public static int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        MySearchWords.limit = limit;
    }


    public void setOutCheck(boolean outCheck) {
        MySearchWords.outCheck = outCheck;
    }

    public static boolean isOutCheck() {
        return outCheck;
    }


    public void start() {
        System.out.println("Searching started...");
        long start = System.nanoTime();
        mainPhaser = new Phaser(1);
        try {
            for (int i = 0; i < arrUrl.length; i++) {
                try {
                    tempURL = new URL(arrUrl[i]);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                executor.execute(new ReadPageContent(tempURL, 1, mainPhaser, arrSearch));
                chackedUrl.add(tempURL);
            }
            mainPhaser.arriveAndAwaitAdvance();
        } finally {
            executor.shutdown();
        }
        long finish = System.nanoTime();
        System.out.println("Searching complete...");
        System.out.println("Time of search = " + (finish - start) / 1000000000 + " s");

        for (ResultListener q : listeners) {
            q.resultSearch(new ResultEvent(UrlWords));
        }
    }
}
